#include "drawsettings.h"

drawSettings::drawSettings(cameraPosition camPos) : camPos_(camPos) {

  modelMat_ = glm::rotate(glm::mat4(1.0f), static_cast<float>(M_PI) / 4,
                          glm::vec3(1.0f, 0.0f, 0.0f));

  //  normalMat_ = glm::mat4(1.0f);
  projMat_ = glm::perspective(glm::radians(fieldOfView),
                              static_cast<float>(dispWidth) /
                                  static_cast<float>(dispHeight),
                              nearClipSurface, farClipSurface);
  viewMat_ = glm::translate(glm::mat4(1.0f),
                            glm::vec3(camPos_.x, camPos_.y, -camPos_.z));

  mvMat_ = viewMat_ * modelMat_;
  normalMat_ = glm::inverseTranspose(mvMat_);

  // NOTE: AmbientCol and diffuseCol are the combined colour of both the light
  // and the object's material properties
  lightPos_ = {camPos_.x + 50.0f, camPos.y + 80.0f, camPos.z};
  ambientCol_ = {0.9f, 0.9f, 0.9f};
  diffuseCol_ = {1.2f, 1.2f, 1.2f};
  cubeRotAxis = {1.0f, 0.0f, 0.0f};

  cubeAngVel = 0.75f; // can be used in future to change rotation speed
}

void drawSettings::updateSettings(float elapsedTime) {
  // Upload the shader uniforms

  modelMat_ = glm::rotate(modelMat_, cubeAngVel * elapsedTime, cubeRotAxis);

  modelMat_ = glm::rotate(modelMat_, cubeAngVel * elapsedTime,
                          glm::vec3(0.0f, 1.0f, 0.0f));

  // Set up the camera

  // viewMat_ = glm::translate(glm::mat4(1.0f),
  //                          glm::vec3(-camPosX_, -camPosY_, -camPosZ_));

  mvMat_ = viewMat_ * modelMat_;

  normalMat_ = glm::inverseTranspose(mvMat_);
}

void drawSettings::updateSettings(float rotVel, glm::vec3 &axisVector) {

  modelMat_ = glm::rotate(modelMat_, cubeAngVel * rotVel, axisVector);

  mvMat_ = viewMat_ * modelMat_;

  normalMat_ = glm::inverseTranspose(mvMat_);
}

void drawSettings::updateSettings(glm::vec3 &movVec) {

  viewMat_ = glm::translate(glm::mat4(1.0f), movVec);

  mvMat_ = viewMat_ * modelMat_;

  normalMat_ = glm::inverseTranspose(mvMat_);
}

void drawSettings::updateCamPos(cameraPosition &newCamPos) {
  camPos_.x += newCamPos.x;
  camPos_.y += newCamPos.y;
  camPos_.z += newCamPos.z;
};

glm::mat4 &drawSettings::getMvMat() { return mvMat_; };
glm::mat4 &drawSettings::getNormalMat() { return normalMat_; };
glm::mat4 &drawSettings::getProjMat() { return projMat_; };
glm::vec3 &drawSettings::getLightPos() { return lightPos_; };
glm::vec3 &drawSettings::getAmbientCol() { return ambientCol_; };
glm::vec3 &drawSettings::getDiffuseCol() { return diffuseCol_; }

cameraPosition &drawSettings::getCamPos() { return camPos_; };
