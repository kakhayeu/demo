#include "texturehandler.h"

textureHandler::textureHandler(std::string fileName) {
  std::ifstream texturesNames(fileName);
  if (!texturesNames) {
    std::cerr << "Couldn't load file with textures names";
    throw "Couldn't load file with textures names";
  }

  while (texturesNames) {
    std::string str;
    std::getline(texturesNames, str);
    if (str != "") {
      textures_[str] = 0;
    }
  }

  for (auto &i : textures_) {
    // Load the texture
    GLuint texture = texLoad(i.first.c_str());
    if (!texture) {
      SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error",
                               "Couldn't load texture.", NULL);
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    i.second = texture;
  }
}

GLuint textureHandler::getTexture(std::string name) const {
  if (textures_.count(name) != 0) {
    return textures_.at(name);
  } else {
    return 0;
  }
}

textureHandler::~textureHandler() {
  for (auto i : textures_) {
    texDestroy(i.second);
  }
}
