#pragma once

#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <vector>

const unsigned int dispWidth = 1280;
const unsigned int dispHeight = 720;
const float nearClipSurface = 1.0f;
const float farClipSurface = 1000.0f;
const float fieldOfView = 60.0f;

/** Encapsulates the data for a single vertex
 *Must match the vertes shader's input
 *
 */
struct Vertex {
  float position[3];
  float texCoord[2];
  float normal[3];
};

struct cameraPosition {
  float x;
  float y;
  float z;
};

std::vector<Vertex> readMesh(const char *fileName);

struct initParams { // Not in use
  const unsigned int disp_width;
  const unsigned int disp_height;
  // TODO add all variables to construct engine
};
