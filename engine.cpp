#include "engine.h"

static bool alreadyExist = false;

engine *engine::createEngine() {
  if (alreadyExist) {
    throw std::runtime_error("engine already exist");
  }
  static engine *result = new engine();

  if (result->initFailed()) {
    std::cerr << "Engine creation failed" << std::endl;
    return nullptr;
  } else {
    alreadyExist = true;
    return result;
  }
}

void destroyEngine(engine *e) {
  if (alreadyExist == false) {
    throw std::runtime_error("engine not created");
  }
  if (e == nullptr) {
    throw std::runtime_error("e is nullptr");
  }
  delete e;
}

engine::engine() : initFailed_(false) {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    SDL_Log("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    initFailed_ = true;
  }

  atexit(SDL_Quit);

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  window = SDL_CreateWindow("GLES3+SDL2 Tutorial", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, dispWidth, dispHeight,
                            SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
  if (!window) {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error",
                             "Couldn't create the main window.", NULL);
    initFailed_ = true;
  }

  context = SDL_GL_CreateContext(window);
  if (!context) {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error",
                             "Couldn't create an OpenGL context.", NULL);
    initFailed_ = true;
  }

  // Enable and set up the depth buffer
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClearDepthf(1.0f);

  // Load shader program and set it for use

  shaderProg = shaderProgLoad("texture.vert", "texture.frag");
  if (!shaderProg) {
    // Error message already displayed...
    initFailed_ = true;
  }

  glUseProgram(shaderProg);

  // Bind texSampler to unit 0

  texSamplerUniformLoc = glGetUniformLocation(shaderProg, "texSampler");
  if (texSamplerUniformLoc < 0) {
    SDL_Log("ERROR: Couldn't get texSampler's location.");
    initFailed_ = true;
  }

  // Get other uniform locations

  mvMatLoc = glGetUniformLocation(shaderProg, "mvMat");
  if (mvMatLoc < 0) {
    SDL_Log("ERROR: Couldn't get mvMat's location.");
    initFailed_ = true;
  }

  normalMatLoc = glGetUniformLocation(shaderProg, "normalMat");
  if (normalMatLoc < 0) {
    SDL_Log("ERROR: Couldn't get normalMat's location.");
    initFailed_ = true;
  }

  projMatLoc = glGetUniformLocation(shaderProg, "projMat");
  if (projMatLoc < 0) {
    SDL_Log("ERROR: Couldn't get projMat's location.");
    initFailed_ = true;
  }

  lightPosLoc = glGetUniformLocation(shaderProg, "lightPos");
  if (lightPosLoc < 0) {
    SDL_Log("ERROR: Couldn't get lightPos's location.");
    initFailed_ = true;
  }

  ambientColLoc = glGetUniformLocation(shaderProg, "ambientCol");
  if (ambientColLoc < 0) {
    SDL_Log("ERROR: Couldn't get ambientCol's location.");
    initFailed_ = true;
  }

  diffuseColLoc = glGetUniformLocation(shaderProg, "diffuseCol");
  if (diffuseColLoc < 0) {
    SDL_Log("ERROR: Couldn't get diffuseCol's location.");
    initFailed_ = true;
  }
}

void engine::render(gameObject &object) {

  glBindBuffer(GL_ARRAY_BUFFER, object.getVbo());
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, object.getIbo());
  glBindTexture(GL_TEXTURE_2D, object.getTexture());

  // Set up for rendering the triangle (activate the VBO)
  GLuint positionIdx = 0; // Position of the vertex attribute 0
  glVertexAttribPointer(positionIdx, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (const GLvoid *)0);
  glEnableVertexAttribArray(positionIdx);

  GLuint texCoordIdx = 1; // TexCoord is vertex attribute 1
  glVertexAttribPointer(texCoordIdx, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (const GLvoid *)offsetof(Vertex, texCoord));
  glEnableVertexAttribArray(texCoordIdx);

  GLuint normalIdx = 2; // Normal is vertes attribute 2
  glVertexAttribPointer(normalIdx, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (const GLvoid *)offsetof(Vertex, normal));
  glEnableVertexAttribArray(normalIdx);

  glUniform1i(texSamplerUniformLoc, 0);
  glUniformMatrix4fv(mvMatLoc, 1, GL_FALSE,
                     glm::value_ptr(object.getSettings().getMvMat()));
  glUniformMatrix4fv(normalMatLoc, 1, GL_FALSE,
                     glm::value_ptr(object.getSettings().getNormalMat()));
  glUniformMatrix4fv(projMatLoc, 1, GL_FALSE,
                     glm::value_ptr(object.getSettings().getProjMat()));
  glUniform3fv(lightPosLoc, 1,
               glm::value_ptr(object.getSettings().getLightPos()));
  glUniform3fv(ambientColLoc, 1,
               glm::value_ptr(object.getSettings().getAmbientCol()));
  glUniform3fv(diffuseColLoc, 1,
               glm::value_ptr(object.getSettings().getDiffuseCol()));

  glDrawElements(GL_TRIANGLES, object.getIndicesCount(), GL_UNSIGNED_SHORT,
                 (GLvoid *)0);

  glDisableVertexAttribArray(object.getVbo()); // Not sure about this call
  glDisableVertexAttribArray(object.getIbo()); // Not sure about this call
};

void engine::swap_buffers() {
  SDL_GL_SwapWindow(window);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

engine::~engine() {
  ShaderProgDestroy(shaderProg);
  shaderProg = 0;
};

bool engine::initFailed() const { return initFailed_; };
