#pragma once
#include "texture.h"
#include <GLES3/gl3.h>
#include <SDL2/SDL.h>
#include <fstream>
#include <iostream>
#include <map>
#include <string>

class textureHandler {
public:
  textureHandler(std::string fileName);
  GLuint getTexture(std::string name) const;
  ~textureHandler();

private:
  std::map<std::string, GLuint> textures_;
};
