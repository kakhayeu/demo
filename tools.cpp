#include "tools.h"

std::vector<Vertex> readMesh(const char *fileName) {
  std::ifstream meshSource(fileName);
  if (!meshSource) {
    std::cerr << "Couldn't load file with mesh";
    throw "Couldn't load file with mesh";
  }

  std::vector<Vertex> result;
  Vertex tmp;

  while (meshSource) {
    meshSource >> tmp.position[0];
    meshSource >> tmp.position[1];
    meshSource >> tmp.position[2];

    meshSource >> tmp.texCoord[0];
    meshSource >> tmp.texCoord[1];

    meshSource >> tmp.normal[0];
    meshSource >> tmp.normal[1];
    meshSource >> tmp.normal[2];

    result.push_back(tmp);
  }

  result.pop_back();
  return result;
}
