#include "engine.h"
#include "fpscounter.h"
#include "gameobject.h"
#include "inputhandle.h"
#include "shader.h"
#include "texture.h"
#include "texturehandler.h"
#include "tools.h"
#include <SDL.h>
#include <SDL_opengles2.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <memory>

// TODO refactor engine to make it singletone..........................done
//      move mesh creating function to separate file...................done
//      implement function to load settings from file..................
//      implement Logger...............................................
//      read about using vertex shader to create many objects..........
//      create demo scene instead of gameplay to show all abilities ...
//      replace disp size from engine and tools to one place...........done
//      need to pass a structure to engine constructor to set
//      building parameters (all variables)............................
//      remove dependence between fps and objects rotation speed.......done
//      forbid copying of drawSettings objects.........................
//      write copy constructor for gameObject..........................
//      add ability to move source of light............................done
//      find out if the light is implemented correctly (change its pos)
//      add ability to change texture of object........................done
//      implement class textureHolder to load and keep all textures....done
//      move all global variables to the separate class "settings".....
//      move all textures to one class "textureHandler"................done
//      implement ability to move camera...............................done
//      implement ability to rotate camera.............................

bool continue_loop = true;
size_t objectIndex = 0;
rotationAxis currentAxis = rotationAxis::X;

int main(/*int argc, char *args[]*/) {
  std::unique_ptr<engine, void (*)(engine *)> eng(engine::createEngine(),
                                                  destroyEngine);
  if (eng == nullptr)
    return EXIT_FAILURE;

  std::vector<Vertex> mesh = readMesh("mesh.txt");

  std::unique_ptr<textureHandler> allTextures =
      std::make_unique<textureHandler>("textures.txt");

  std::vector<std::unique_ptr<gameObject>> objects;

  for (size_t i = 0; i < 5; i++) {
    // add code to choose texture
    std::string textureName;
    if (i % 2 == 0) {
      textureName = "crate2_diffuse.png";
    } else {
      textureName = "crate1_diffuse.png";
    }
    objects.push_back(std::make_unique<gameObject>(
        mesh, cameraPosition({-600.f + 300 * i, 150.f, 800.f}),
        allTextures->getTexture(textureName)));
    if (objects[i]->initFailed()) {
      std::cerr << "Object creation failed" << std::endl;
      return EXIT_FAILURE;
    }
  }

  std::unique_ptr<fpsCounter> fps = std::make_unique<fpsCounter>();

  // Main game loop

  while (continue_loop) {

    fps->count();
    SDL_Event event;

    if (SDL_PollEvent(&event) != 0) {
      handleInput(objects, event, *allTextures);
    }
    // Apply changes to the gameObject

    // Now draw!
    for (size_t i = 0; i < 5; i++) {
      eng->render(*objects[i]);
    }

    // Update the window
    eng->swap_buffers();
  }

  return EXIT_SUCCESS;
}
