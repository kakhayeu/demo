#pragma once

#include "engine.h"
#include "fpscounter.h"
#include "gameobject.h"
#include "shader.h"
#include "texture.h"
#include "texturehandler.h"
#include "tools.h"
#include <SDL.h>
#include <SDL_opengles2.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <memory>

extern bool continue_loop;
extern size_t objectIndex;
extern rotationAxis currentAxis;

void handleInput(std::vector<std::unique_ptr<gameObject>> &objects,
                 SDL_Event event, const textureHandler &allTextures);
