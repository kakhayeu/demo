#pragma once

#include <SDL.h>
#include <iostream>

class fpsCounter {
public:
  fpsCounter();
  void count();
  float getElapsedFrames();

private:
  Uint32 fpsTimer;
  Uint32 nbFrames;
  Uint32 currTime;
};
