#pragma once

#include <GLES3/gl3.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengles2.h>
#include <cstdio>
#include <cstdlib>

/* Load a vertex and fragment sahder from disk and compiles (& links) them
 *into a shader program
 *
 *This will print any errors to the cinsole.
 *
 *@param vertFilename filename for the vertex shader
 *@param fragFilename the fragment shader's filename.
 *
 *@return GLuint the shader program's ID, or 0 if failed.
 */

GLuint shaderProgLoad(const char *vertFilename, const char *fragFilename);

/*
 *Destroys a shader program
 */

void ShaderProgDestroy(GLuint shaderProg);
