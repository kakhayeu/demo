#pragma once

#include "tools.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

class drawSettings {
public:
  drawSettings(cameraPosition camPos);

  void updateSettings(float elapsedTime);
  void updateSettings(float rotVel, glm::vec3 &axisVector);
  void updateSettings(glm::vec3 &movVec);
  void updateCamPos(cameraPosition &newCamPos);

  glm::mat4 &getMvMat();
  glm::mat4 &getNormalMat();
  glm::mat4 &getProjMat();
  glm::vec3 &getLightPos();
  glm::vec3 &getAmbientCol();
  glm::vec3 &getDiffuseCol();
  cameraPosition &getCamPos();
  //  drawSettings getSettings();

private:
  glm::mat4 modelMat_;
  glm::mat4 mvMat_;
  glm::mat4 normalMat_;
  glm::mat4 projMat_;
  glm::mat4 viewMat_;

  glm::vec3 lightPos_;
  glm::vec3 ambientCol_;
  glm::vec3 diffuseCol_;
  glm::vec3 cubeRotAxis;
  // NOTE: OpenGL camera look down the negative z-axis
  cameraPosition camPos_;
  float cubeAngVel; // Radians/s
};
