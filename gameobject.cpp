#include "gameobject.h"
#include "texture.h"
#include <iostream>

gameObject::gameObject(std::vector<Vertex> &vertices, cameraPosition camPos,
                       GLuint texture)
    : settings_(camPos), x_(camPos.x), y_(camPos.y), z_(camPos.z),
      vertices_(vertices), texture_(texture), initFailed_(false) {
  const GLsizei vertsPerSide = 4;
  const GLsizei numSides = 6;
  const GLsizei indicesPerSide = 6;
  numIndices_ = indicesPerSide * numSides;

  indices_.resize(numIndices_);

  GLuint i = 0;
  for (GLushort j = 0; j < numSides; ++j) {
    GLushort sideBaseIdx = j * vertsPerSide;
    indices_[i++] = sideBaseIdx + 0;
    indices_[i++] = sideBaseIdx + 1;
    indices_[i++] = sideBaseIdx + 2;
    indices_[i++] = sideBaseIdx + 2;
    indices_[i++] = sideBaseIdx + 3;
    indices_[i++] = sideBaseIdx + 0;
  }
  init();
};

float gameObject::getX() const { return x_; }

float gameObject::getY() const { return y_; }

float gameObject::getZ() const { return z_; }

float gameObject::get_w() const { return w_; }

float gameObject::get_h() const { return h_; }

void gameObject::move(float dx, float dy) {
  x_ += dx;
  y_ += dy;
}
gameObject::~gameObject() {
  texDestroy(texture_);
  texture_ = 0;
  vboFree(vbo_);
  vbo_ = 0;
  iboFree(ibo_);
  ibo_ = 0;
}

void gameObject::setX(float dX) { x_ += dX; }
void gameObject::setY(float dY) { y_ += dY; }
void gameObject::setZ(float dZ) { z_ += dZ; }

void gameObject::setIbo() {
  ibo_ = iboCreate(indices_.data(), indices_.size());
  if (!ibo_) {
    // Failed. Error message has already been printed, so just quit
    initFailed_ = true;
  }

  // Bind the index array for use
  // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);
  glDisableVertexAttribArray(ibo_);
}

void gameObject::setVbo() {
  vbo_ = vboCreate(vertices_.data(), vertices_.size());
  if (!vbo_) {
    // Failed. Error message already been printed, so just quit
    initFailed_ = true;
  }

  // Bind the vertex array for use
  // glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glDisableVertexAttribArray(vbo_);
}

/**Creates the Index Buffer Object (IBO) containing
 *the given indices.
 *
 *@param indices pointer to the array of indices
 *@param numIndices the number of indices in the array
 */
GLuint gameObject::iboCreate(GLushort *indices, GLuint numIndices) {
  // Create the Index Buffer Object
  GLuint ibo;
  int nBuffers = 1;
  glGenBuffers(nBuffers, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

  // Copy the index data in, and deactivate
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * numIndices,
               indices, GL_STATIC_DRAW);
  // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // Check for problems
  GLenum err = glGetError();
  if (err != GL_NO_ERROR) {
    // Failed
    glDeleteBuffers(nBuffers, &ibo);
    SDL_Log("Creating IBO failed, code %u\n", err);
    ibo = 0;
  }
  return ibo;
}

/**Frees the IBO.
 *
 *@param vbo the ibo's name.
 */

void gameObject::iboFree(GLuint ibo) { glDeleteBuffers(1, &ibo); }

/*Creates the Vertex Buffer Object (VBO) containing
 * the given vertices.
 *
 * @param vertices pointer to the array of vertices
 * @param numVertices the number of vertices in the array
 */
GLuint gameObject::vboCreate(const Vertex *vertices, GLuint numVertices) {
  // Create the Vertex Buffer Object
  GLuint vbo;
  int nBuffers = 1;
  glGenBuffers(nBuffers, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  // Copy the vertex data in, and deactivate
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * numVertices, vertices,
               GL_STATIC_DRAW);
  // glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Check for problems
  GLenum err = glGetError();
  if (err != GL_NO_ERROR) {
    // Failed
    glDeleteBuffers(nBuffers, &vbo);
    SDL_Log("Creating VBO failed, code %u\n", err);
    vbo = 0;
  }
  return vbo;
};

/*
Frees to VBO
*
*@param vbo the VBO's name.

*/
void gameObject::vboFree(GLuint vbo) { glDeleteBuffers(1, &vbo); };

// void gameObject::setTexture(const char* fileName)
//{
//    if (texture_ != 0) {
//        texDestroy(texture_);
//    }

//    // Load the texture
//    texture_ = texLoad(fileName);
//    if (!texture_) {
//        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error",
//            "Couldn't load texture.", NULL);
//        initFailed_ = true;
//    }

//    // Bind the texture to unit 0
//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, texture_);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//};

void gameObject::setTexture(GLuint name) {

  //    glActiveTexture(GL_TEXTURE0);
  texture_ = name;

  glBindTexture(GL_TEXTURE_2D, name);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

// Looks not cool to change object using getter
// but it was made as a temporary quick fix
// and it still doesn't work

void gameObject::setLightPos(glm::vec3 lightPos) {
  settings_.getLightPos().x += lightPos.x;
  settings_.getLightPos().y += lightPos.y;
  settings_.getLightPos().z += lightPos.z;
}

void gameObject::setCamPos(cameraPosition newCamPos) {
  settings_.updateCamPos(newCamPos);
  glm::vec3 v{
      settings_.getCamPos().x,
      settings_.getCamPos().y,
      settings_.getCamPos().z,
  };
  settings_.updateSettings(v);
}

size_t gameObject::getIndicesCount() const { return numIndices_; }

void gameObject::init() {
  setVbo();
  setIbo();
  // setTexture();
  vertices_.clear();
  indices_.clear();
};

bool gameObject::initFailed() const { return initFailed_; };

GLuint gameObject::getIbo() const { return ibo_; };
GLuint gameObject::getVbo() const { return vbo_; };
GLuint gameObject::getTexture() const { return texture_; };

void gameObject::update(float delta) { settings_.updateSettings(delta); }

void gameObject::rotate(float angle, rotationAxis axis) {
  glm::vec3 axisVector = glm::vec3(0.0f, 0.0f, 0.0f);
  switch (axis) {
  case X:
    axisVector = glm::vec3(1.0f, 0.0f, 0.0f);
    break;
  case Y:
    axisVector = glm::vec3(0.0f, 1.0f, 0.0f);
    break;
  case Z:
    axisVector = glm::vec3(0.0f, 0.0f, 1.0f);
    break;
  case XY:
    axisVector = glm::vec3(1.0f, 1.0f, 0.0f);
    break;
  case XZ:
    axisVector = glm::vec3(1.0f, 0.0f, 1.0f);
    break;
  case YZ:
    axisVector = glm::vec3(0.0f, 1.0f, 1.0f);
    break;
  case XYZ:
    axisVector = glm::vec3(1.0f, 1.0f, 1.0f);
    break;
  }
  settings_.updateSettings(angle, axisVector);
}

void gameObject::move(float x, float y, float z) {
  glm::vec3 axisVector = glm::vec3(x, y, -z);
  settings_.updateSettings(axisVector);
}

drawSettings &gameObject::getSettings() { return settings_; }
