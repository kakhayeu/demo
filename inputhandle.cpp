#include "inputhandle.h"

void handleInput(std::vector<std::unique_ptr<gameObject>> &objects,
                 SDL_Event event, const textureHandler &allTextures) {
  switch (event.type) {
  case SDL_QUIT:
    continue_loop = false;
    break;
  case SDL_KEYDOWN:
    switch (event.key.keysym.sym) {
    case SDLK_0:
      objectIndex = 0;
      break;
    case SDLK_1:
      objectIndex = 1;
      break;
    case SDLK_2:
      objectIndex = 2;
      break;
    case SDLK_3:
      objectIndex = 3;
      break;
    case SDLK_4:
      objectIndex = 4;
      break;
    case SDLK_w:
      objects[objectIndex]->setY(5);
      objects[objectIndex]->move(objects[objectIndex]->getX(),
                                 objects[objectIndex]->getY(),
                                 objects[objectIndex]->getZ());
      break;
    case SDLK_s:
      objects[objectIndex]->setY(-5);
      objects[objectIndex]->move(objects[objectIndex]->getX(),
                                 objects[objectIndex]->getY(),
                                 objects[objectIndex]->getZ());
      break;
    case SDLK_a:
      objects[objectIndex]->setX(-5);
      objects[objectIndex]->move(objects[objectIndex]->getX(),
                                 objects[objectIndex]->getY(),
                                 objects[objectIndex]->getZ());
      break;
    case SDLK_d:
      objects[objectIndex]->setX(5);
      objects[objectIndex]->move(objects[objectIndex]->getX(),
                                 objects[objectIndex]->getY(),
                                 objects[objectIndex]->getZ());
      break;
    case SDLK_e:
      objects[objectIndex]->setZ(5);
      objects[objectIndex]->move(objects[objectIndex]->getX(),
                                 objects[objectIndex]->getY(),
                                 objects[objectIndex]->getZ());
      break;
    case SDLK_q:
      objects[objectIndex]->setZ(-5);
      objects[objectIndex]->move(objects[objectIndex]->getX(),
                                 objects[objectIndex]->getY(),
                                 objects[objectIndex]->getZ());
      break;

      // Rotating camera by 3 axes

    case SDLK_UP:
      for (auto &i : objects) {
        i->setCamPos(cameraPosition({0, -5, 0}));
        i->move(i->getSettings().getCamPos().x, i->getSettings().getCamPos().y,
                i->getSettings().getCamPos().z);
      }
      break;

    case SDLK_DOWN:
      for (auto &i : objects) {
        i->setCamPos(cameraPosition({0, 5, 0}));
        i->move(i->getSettings().getCamPos().x, i->getSettings().getCamPos().y,
                i->getSettings().getCamPos().z);
      }
      break;

    case SDLK_LEFT:
      for (auto &i : objects) {
        i->setCamPos(cameraPosition({5, 0, 0}));
        i->move(i->getSettings().getCamPos().x, i->getSettings().getCamPos().y,
                i->getSettings().getCamPos().z);
      }
      break;

    case SDLK_RIGHT:
      for (auto &i : objects) {
        i->setCamPos(cameraPosition({-5, 0, 0}));
        i->move(i->getSettings().getCamPos().x, i->getSettings().getCamPos().y,
                i->getSettings().getCamPos().z);
      }
      break;

    case SDLK_o:
      for (auto &i : objects) {
        i->setCamPos(cameraPosition({0, 0, 5}));
        i->move(i->getSettings().getCamPos().x, i->getSettings().getCamPos().y,
                i->getSettings().getCamPos().z);
      }
      break;

    case SDLK_p:
      for (auto &i : objects) {
        i->setCamPos(cameraPosition({0, 0, -5}));
        i->move(i->getSettings().getCamPos().x, i->getSettings().getCamPos().y,
                i->getSettings().getCamPos().z);
      }
      break;

      //////////////////////////////////////////////////////////////////////////////////

      //      commented text can be used to move light source
      //      it works fine except one issue: there are no shadows and all
      //      objects look unnatural so they should be implemented...
      //      someday

      /*
          case SDLK_w:
            for (auto &i : objects) {
              i->setLightPos({0, 5, 0});
            }
            break;
          case SDLK_s:
            for (auto &i : objects) {
              i->setLightPos({0, -5, 0});
            }
            break;
          case SDLK_a:
            for (auto &i : objects) {
              i->setLightPos({-5, 0, 0});
            }
            break;
          case SDLK_d:
            for (auto &i : objects) {
              i->setLightPos({5, 0, 0});
            }
            break;
          case SDLK_e:
            for (auto &i : objects) {
              i->setLightPos({0, 0, 5});
            }
            break;
          case SDLK_q:
            for (auto &i : objects) {
              i->setLightPos({0, 0, -5});
            }
            break;
      */

    case SDLK_r:
      objects[objectIndex]->setTexture(
          allTextures.getTexture("crate1_diffuse.png"));
      break;
    case SDLK_t:
      objects[objectIndex]->setTexture(
          allTextures.getTexture("crate2_diffuse.png"));
      break;
    case SDLK_f:
      objects[objectIndex]->rotate(0.01, currentAxis);
      break;
    case SDLK_x:
      currentAxis = rotationAxis::X;
      break;
    case SDLK_y:
      currentAxis = rotationAxis::Y;
      break;
    case SDLK_z:
      currentAxis = rotationAxis::Z;
      break;
    }
    continue_loop = true;
    break;
  }
  if (event.type == SDL_QUIT) {
    continue_loop = false;
  }
}
