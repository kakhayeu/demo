#include "fpscounter.h"

fpsCounter::fpsCounter()
    : fpsTimer(SDL_GetTicks()), nbFrames(0), currTime(SDL_GetTicks()) {}

void fpsCounter::count() {
  currTime = SDL_GetTicks();
  nbFrames++;

  if (currTime - fpsTimer >= 5000) { // If last cout was more than 5 sec ago
    std::cout << nbFrames << " frames in 5 seconds = " << nbFrames / 5
              << " fps and " << 5000.f / static_cast<float>(nbFrames)
              << " ms/frame" << std::endl;
    nbFrames = 0;
    fpsTimer += 5000;
  }
}

float fpsCounter::getElapsedFrames() { return nbFrames; }
