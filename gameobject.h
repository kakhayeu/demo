#pragma once

#include "drawsettings.h"
#include "texturehandler.h"
#include "tools.h"
#include <GLES3/gl3.h>
#include <SDL.h>
#include <glm/glm.hpp>
#include <vector>

enum rotationAxis { X, Y, Z, XY, XZ, YZ, XYZ };

class gameObject {
public:
  // gameObject(float size);
  gameObject(std::vector<Vertex> &vertices, cameraPosition camPos,
             GLuint texture);
  ~gameObject();

  float getX() const;
  float getY() const;
  float getZ() const;
  float get_w() const;
  float get_h() const;
  GLuint getIbo() const;
  GLuint getVbo() const;
  GLuint getTexture() const; // not in use yet
  size_t getIndicesCount() const;

  void setX(float dX);
  void setY(float dY);
  void setZ(float dZ);
  void setIbo();
  void setVbo();
  // void setTexture(const char* fileName = "crate2_diffuse.png");
  void setTexture(GLuint);
  void setLightPos(glm::vec3 lightPos);
  void setCamPos(cameraPosition newCamPos);

  void move(float dx, float dy);
  void init();
  bool initFailed() const;
  void update(float delta);
  void rotate(float angle, rotationAxis axis);
  void move(float x, float y, float z);
  drawSettings &getSettings();

private:
  GLuint iboCreate(GLushort *indices, GLuint numIndices);
  void iboFree(GLuint ibo);

  GLuint vboCreate(const Vertex *vertices, GLuint numVertices);
  void vboFree(GLuint vbo);

  drawSettings settings_;
  float x_;
  float y_;
  float z_;
  float w_; // not in use yet
  float h_; // not in use yet
  std::vector<Vertex> vertices_;
  std::vector<GLushort> indices_;
  GLuint ibo_;
  GLuint vbo_;
  GLuint texture_;
  bool initFailed_;
  GLsizei numIndices_;
};
