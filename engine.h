#pragma once

#include "gameobject.h"
#include "scene.h"
#include "shader.h"
#include "texture.h"
#include "tools.h"
#include <GLES3/gl3.h>
#include <SDL.h>
#include <SDL_opengles2.h>
#include <cstdio>
#include <cstdlib>
#include <endian.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <stdexcept>
#include <vector>

extern const unsigned int F;
extern const unsigned int dispHeight;

class engine {
public:
  static engine *createEngine();
  ~engine();

  void swap_buffers();

  void render(gameObject &object);

  bool initFailed() const;

private:
  engine();
  SDL_Window *window = NULL;
  SDL_GLContext context = NULL;
  GLuint shaderProg;
  GLint mvMatLoc;
  GLint normalMatLoc;
  GLint projMatLoc;
  GLint lightPosLoc;
  GLint ambientColLoc;
  GLint diffuseColLoc;
  GLint texSamplerUniformLoc;
  bool initFailed_;
};

void destroyEngine(engine *);
